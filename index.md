# Indekseerimine (Index)

Programmeerimiskeeltes tähendab indeks positsiooninumbrit, mis näitab elemendi asukohta erinevates andmestruktuurides.

Python on 0-indeksiga programmeerimiskeel. See tähendab, et andmestruktuuri esimene element asub indeksil 0.

Pythonis järgnevas näites järjendis `fruits` omab sõne `apple` indeksit 0 ja sõne `cherry` indeksit 2.

```
fruits = ['apple', 'banana', 'cherry']
print(fruits[0]) # 'apple'
print(fruits[2]) # ´cherry`
```

Sarnaselt sõnes `apple` omab täht `a` indeksit 0 ja täht `e` indeksit 4.

```
print("apple"[0]) # 'a'
print("apple"[4]) # 'e'
```

Väärtusi saame ka otse eraldada iga andmestruktuuri alamstruktuurist.

```
arr = [[1, 2, 3], [4, 5, 6]] 
print(arr[1][2]) # 6
fruits = ['apple', 'banana', 'cherry']
print(fruits[0][4]) # 'e'
```

## Negatiivne indekseerimine

Pythonis saab kasutada ka negatiivset indekseerimist andmestruktuuri elementidele juurdepääsemiseks. Negatiivsed indekseid loetakse andmestruktuuri lõpust. Viimast elementi andmestruktuuris tähistab -1, eelviimast -2 jne...

```
fruits = ['apple', 'banana', 'cherry'] 
print(fruits[-1]) # 'cherry' 
print(fruits[-2]) # 'banana' 
print(fruits[-3]) # 'apple'
```

## Tükeldamine kasutades indekseid (Slicing)

Python võimaldab ligipääseda mitmele andmestruktuuri elemendile tükeldamise abil. Tükeldamine kasutab süntaksit `[algus:lõpp:samm]`, kus:

- `algus` on indeks, millest alustatake tükeldamist.
- `lõpp` on indeks, milleni tükeldatakse (viimast elementi ei kaasata vahemikku).
- `samm` on täisarv, mis tähistab sammu suurust algusest.

Näiteks teise ja kolmanda (indeksiga 1 ja 2) elemendi valimiseks järjendist `fruits` saaks kasutada järgnevat süntaksi:

```
fruits = ['apple', 'banana', 'cherry', 'date', 'pear'] 
print(fruits[1:3]) # ['banana', 'cherry']
```

Kui jätta välja `algus`, siis Python eeldab, et soovite alustada andmestruktuuri algusest.

```
fruits = ['apple', 'banana', 'cherry'] 
print(fruits[:2]) # ['apple', 'banana']
language = 'Python'
print(language[:4]) # 'Pyth'
```

Kui jätta välja `lõpp`, siis Python eeldab, et soovite valida kõik elemendid anmdestruktuuri lõpuni.

```
fruits = ['apple', 'banana', 'cherry'] 
print(fruits[1:]) # ['banana', 'cherry']
language = 'Python'
print(language[1:]) # 'ython'
```

Kui jätta välja `samm`, siis valitakse iga element tükeldamise vahemikus. 
Väärtuse määramise korral valitakse iga x sammu tagant uus element.

```
my_list = [1, 2, 3, 4, 5, 6] 
print(my_list[::2]) # [1, 3, 5]
print(my_list[::-2]) # [6, 4, 2]
```

## Indeksi asukoha leidmine

Pythonis on võimalik kasutada sisseehitatud meetodit index(), mis tagastab antud elemendi esimese esinemise indeksi.

```
fruits = ['apple', 'banana', 'cherry'] 
print(fruits.index('banana')) # 1
print(fruits[0].index('e')) # 4
```

Kui elementi ei leita järjendist, tõstatab Python erindi `ValueError`.
